import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
/**
 * @customElement
 * @polymer
 */

class VisorCuentas extends PolymerElement {
  static get template() {
    return html`

      <style>
          :host {
            display: block;
            border: solid blue;
          }
      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <div class="row greybg">
          <div class="col-5">IBAN</div>
          <div class="col-1">Saldo</div>
      </div>
      
          <template is="dom-repeat" items="[[cuentas]]">
              <div class="row greybg">
                  <div class="col-5">[[item.IBAN]]</div>
                  <div class="col-1">[[item.balance]] €</div>
              </div>
          </template>


      <iron-ajax
        auto
        id="getAccounts"
        url="http://localhost:3000/apitechu/v1/accounts/{{idusuario}}"
        handle-as="json"
        on-response="showData"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      idusuario: {
        type: Number
      },

      cuentas: {
        type: Array
      }

    };
  } // End properties

  showData(data) {
    console.log("showData");
    console.log(data.detail.response);

    this.cuentas = data.detail.response;
    // this.last_name = data.detail.response.IBAN;
    // this.email = data.detail.response.balance;

  }

  showError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.request.xhr.response);
  }
} // End class

window.customElements.define('visor-cuentas', VisorCuentas);
