import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`

      <style>
        :host {
          display: block;
        }
      </style>
      <input type="email" placeholder="email" value="{{email::input}}"/>
      <br />
      <br />
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br />
      <br />
      <button on-click="login">Login</button>
      <span hidden$="[[!isLogged]]">Bienvenido/a de nuevo</span>
      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login/"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXResponse"
        on-error="showError"
      >
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      password: {
        type: String
      },email: {
        type: String
      }, isLogged:{
        type: Boolean,
        value: false
      }
    };
  } // End properties

  login() {

    console.log("El usuario ha pulsado el botón");

    var loginData = {
        "email":this.email,
        "password":this.password
    }
 console.log(JSON.stringify(loginData));
    this.$.doLogin.body = JSON.stringify(loginData);
    this.$.doLogin.generateRequest();

    console.log(loginData); //no funcionaría si hicieramos console.log("Login del usuario" + loginData);

  }

  manageAJAXResponse(data) {

    console.log("Llegaron los resultados");
    console.log(data.detail.response);

    this.isLogged = true;

  }

  showError(error) {
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.request.xhr.response);
  }

} // End class

window.customElements.define('login-usuario', LoginUsuario);
