import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */

class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*all:initial; <!-- indica que inicializamos todo, por lo que no heredará estilos globales-->*/
          border: solid blue;
        }
        .redbg {
          background-color: red;
        }

        .greenbg {
          background-color: green;
        }

        .bluebg {
          background-color: blue;
        }
        .greybg {
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <br />
      <div class="row greybg">
      <!-- col define la columna para tamaño xs, sm para sm, md para md, lg para lg y xl para xl -->
      <!-- <div class="col-2 col-sm-6 redbg">col1</div> -->
          <div class="col-1 offset-1 col-sm-1 redbg">col1</div>
          <div class="col-1 offset-1 greenbg">col2</div>
          <div class="col-4 offset-1 bluebg">col3</div>
      </div>

      <button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>
      <button class="btn btn-danger">Login</button>
      <button class="btn btn-warning">Logout</button>
      <button class="btn btn-primary btn-lg">Login</button>
      <button class="btn btn-secondary btn-sm">Logout</button>
      <button class="btn btn-ligth">Login</button>
      <button class="btn btn-dark">Logout</button>

      <h2>Soy [[first_name]] [[last_name]] </h2>
      <h2>y mi email es [[email]] </h2>

      <iron-ajax
        auto
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{id}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },last_name: {
        type: String
      },email: {
        type: String
      },id: {
        type: Number
      }
    };
  } // End properties

  showData(data) {
    console.log("showData");
    console.log(data.detail.response);
    console.log(data.detail.response.first_name);

    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;

  }

} // End class

window.customElements.define('visor-usuario', VisorUsuario);
